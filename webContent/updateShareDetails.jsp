<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>User Management Application</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body  align="center" class="bg-image"
	style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/76.jpg'); height: 100vh">
	<div class="container col-md-5">
		<div class="card" >
			<div class="card-body "style= "background: #f7dcb8;">
			
			<h6 style="color: #55efa8 !important;"
		        class="text-decoration-underline">Update the share price of the ${CompanyDetails.companyName}</h6>
		        
				<form action="ShareController" method="post">


						<input type="hidden" name="id"
							value="<c:out value='${CompanyDetails.companyId}' />" />

					<fieldset class="form-group">
						<label>company Name</label> <input type="text" readonly
							value="<c:out value='${CompanyDetails.companyName}' />" class="form-control"
							name="companyName" required="required">
					</fieldset>

					<fieldset class="form-group">
						<label>Share price</label> <input type="text"
							value="<c:out value='${CompanyDetails.sharePrice}' />" class="form-control"
							name="sharePrice">
					</fieldset>
					
					<input type="hidden"
							name="command" value="Update" />

					

					<button type="submit" class="btn btn-success">Save</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

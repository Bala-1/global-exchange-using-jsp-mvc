<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="model.*,java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Company shares</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
	integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
	integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
	crossorigin="anonymous"></script>

</head>
<body align="center">

	<div class="container-fluid  p-1 mt-2">
		<div class="row">
			<div class="col-4">
				<img
					src="https://www.clipartmax.com/png/middle/298-2989959_global-exchange-global-exchange-ltd.png"
					height="60%" width="100%" alt="logo">
			</div>
			<div class="col-4"></div>
			<div class="col-4">
				<form action="ShareController" method="post">
					<input type="hidden" name="command" value="Logout" />
					<button type="submit" class="btn btn-success m-4"
						style="float: right;">logout</button>

				</form>
			</div>
		</div>
	</div>
	<input type="hidden" name="command" />

	<h6 style="color: #55efa8 !important;"
		class="text-decoration-underline">Company Share List</h6>

	<form action="ShareController" method="post">
		<input type="hidden" name="command" value="NewCompany" />
		<button type="submit"
			class="btn btn-outline-light text-dark text-decoration-underline">
			<p style="color: deeppink">Enlist a new company</p>
		</button>
	</form>


	<div class="container">
		<div class="row">
			<div class="col-1"></div>
			<div class="col-10">
				<table class="table table-bordered ">
					<tr style="color: royalblue; background-color: orange;">
						<th>Company Id</th>
						<th>Company name</th>
						<th>Share price</th>
						<th colspan=2>Actions</th>
					</tr>
					<c:forEach var="tempShare" items="${ListOfCompanies}">

						<tr>
							<td>${tempShare.companyId}</td>
							<td>${tempShare.companyName}</td>
							<td>${tempShare.sharePrice}</td>
							<td><form action="ShareController" method="post">
									<input type="hidden" name="IdToEdit"
										value="${tempShare.companyId}" /> <input type="hidden"
										name="command" value="Load" />
									<button type="submit"
										class="btn btn-outline-light text-dark text-decoration-underline  m-0">
										<p style="color: deeppink">Edit Share Price</p>
									</button>

								</form></td>
							<td>
								<form action="ShareController" method="post">
									<input type="hidden" name="IdToDelete"
										value="${tempShare.companyId}" /> <input type="hidden"
										name="command" value="Delete" />
									<button type="submit"
										class="btn btn-outline-light text-dark text-decoration-underline">
										<p style="color: deeppink">Delete Company</p>
									</button>
								</form>
							</td>

						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="col-1"></div>

		</div>
	</div>


</body>
</html>
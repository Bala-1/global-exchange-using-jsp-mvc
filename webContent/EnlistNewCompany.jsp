<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Enlist new company</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
	integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
	integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
	crossorigin="anonymous"></script>

</head>
<body align="center" class="bg-image"
	style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/76.jpg'); height: 100vh; margin-left: 410px; padding-right: 430px">
	<h4 class="modal-title">Enlist a new Company</h4>
	<form action="ShareController" method="post">
	
	  <input type="hidden" name="command" value="ADD" />
	  
		<div class="form-floating mb-3 mt-3">
			<input type="text" class="form-control"
				placeholder="Enter Company name" name="companyName" required>
			<label for="companyName">Company Name</label>
		</div>

		<div class="form-floating mt-3 mb-3">
			<input type="text" class="form-control"
				placeholder="Enter Share price" name="sharePrice" required>
			<label for="sharePrice">share Price</label>
		</div>

		<button type="submit" class="btn btn-primary">Add Company</button>
	</form>
	<div><br></div>
	<form action="ShareController" method="post">
					<button type="submit" class="btn btn-primary">Back to List</button>
	
	</form>
	
</body>
</html>
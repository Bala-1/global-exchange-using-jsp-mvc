<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Global Exchange sign in</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</head>
<body>
    <div class="header">
        <div class="container-fluid bg-light p-3 mt-5" >
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <a href=""> <img src="https://www.clipartmax.com/png/middle/298-2989959_global-exchange-global-exchange-ltd.png" height="50%" width="90%" alt="logo"> </a>
    
                </div>
                <div class="col-4" >
                    <div class="createaccount " style="line-height:15%; float:right;padding-right: 35px;" >
                        <h6>New to Global Exchange?</h6><br>
                        
                        <form action="LoginController" method="post">
						 <input type="hidden" name="command" value="LoadCreateAccount" />
						<button type="submit" 	class="btn btn-info "  float="right" >
							Create Account
						</button>
					</form>
					
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-light">
        <div class="signInIntro" style="text-align:center ;">
            <h1>Sign in to Global Exchange</h1>
        </div>
         <%  
       String invalidCred = (String)session.getAttribute("invalidCred");
       if(invalidCred != null){
       %>
		<div class="text-danger" align="center"><h2>${invalidCred} </h2> </div><br><%session.removeAttribute("invalidCred");} %>
        <form action="LoginController" method="post">
        
         <input type="hidden" name="command" value="Login" />
        
        <div class="form" style=" margin-left: 410px; padding-right: 430px">
            <div class="form-floating mb-3 mt-3">
                <input type="text" class="form-control" id="email" placeholder="Enter email" name="email">
                <label for="email">Email</label>
            </div>
              
            <div class="form-floating mt-3 mb-3">
                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd">
                <label for="pwd">Password</label>
            </div>
           
            <button type="submit" class="btn btn-primary">Sign in</button>
        </div>
        </form>
        
        

    </div>
</body>
</html>
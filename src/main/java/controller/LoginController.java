package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.UserDao;
import model.UserModel;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String theCommand = request.getParameter("command");

		// if the command is missing, then default to listing shares
		if (theCommand == null) {
			theCommand = "LoadLogin";
		}
		
		switch (theCommand) {

		case "LoadLogin":
			loadLoginPage(request, response);
			break;
			
		case "Login":
			try {
				loginUser(request, response);
			} catch (ClassNotFoundException | SQLException | ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "LoadCreateAccount":
			loadCreateAccountPage(request, response);
			break;
			
		case "AddUser":
			registerUser(request, response);
			break;

			
		}
	}

	private void registerUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
	// retriving all parameters from the JSP page
		
		String firstName = request.getParameter("FName");
		String lastName = request.getParameter("LName");
		String dob = request.getParameter("DOB");
		String gender = request.getParameter("Gender");
		String accMail = request.getParameter("mail");
		String accPwd = request.getParameter("pwd");
		
		
		HttpSession session = request.getSession();
		UserModel user = new UserModel();
		UserDao userDao = new UserDao();


		if(gender == null){
			 session.setAttribute("genderError", "please enter the gender");
			 response.sendRedirect("UserRegistration.jsp");
		} else
			try {
				if(UserDao.CheckUserExistance(accMail)){
							session.setAttribute("duplicateMail", "email address is already in use");
							 response.sendRedirect("UserRegistration.jsp");
				}else{
							// setting all values to model class
							
							user.setFirstName(firstName);
							user.setLastName(lastName);
							user.setDob(dob);
							user.setGender(gender);
							user.setEmail(accMail);
							user.setPassword(accPwd);
							
							response.getWriter().print(firstName);
							
							//calling a method in Dao class to insert data in table
							
							try {

								userDao.registerUser(user);
								 response.sendRedirect("loginpage.jsp");


							} catch (SQLException | ClassNotFoundException e) {
								response.getWriter().print(e);

								// TODO Auto-generated catch block
								e.printStackTrace();
							}

				}
			} catch (SQLException | IOException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}

	private void loadCreateAccountPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		 response.sendRedirect("UserRegistration.jsp");
		
	}

	private void loginUser(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, ServletException, IOException {
		String uname = request.getParameter("email");
		String psswd = request.getParameter("pswd");
		
		HttpSession session = request.getSession();


		UserModel user = new UserModel();

		user.setEmail(uname);
		user.setPassword(psswd);

		boolean isCredentialsValid = false; 
		 
		isCredentialsValid = UserDao.validateCredentials(user);


		
			if (isCredentialsValid) {
				//response.sendRedirect("ShareController");
				 RequestDispatcher dispatcher = request.getRequestDispatcher("ShareController");
		         dispatcher.forward(request,response);
			} else {
				response.sendRedirect("loginpage.jsp");

				session.setAttribute("invalidCred", "Invalid Credentials");

			}
		
	}		
	

	private void loadLoginPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		 response.sendRedirect("loginpage.jsp");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

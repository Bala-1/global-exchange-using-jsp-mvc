package controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.CompanySharesDao;
import model.CompanyShare;

/**
 * Servlet implementation class ShareController
 */
@WebServlet("/ShareController")
public class ShareController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShareController() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			// read the "command" parameter
			String theCommand = request.getParameter("command");

			// if the command is missing, then default to listing shares
			if (theCommand == null) {
				theCommand = "LIST";
			}

			// route to the appropriate method
			switch (theCommand) {

			case "List":
				listCompanyshares(request, response);
				break;
				
			case "NewCompany":
				getEnlistNewCompanyForm(request, response);
				break;

			case "ADD":
				addCompany(request, response);
				break;

			case "Load":
				loadEditForm(request, response);
				break;

			case "Update":
				updateShareDetails(request, response);
				break;

			case "Delete":
				deleteCompany(request, response);
				break;
				
			case "Logout":
				redirectToLogin(request, response);
				break;


			default:
				listCompanyshares(request, response);
			}

			/*
			 * if(request.getAttribute("command")!= null) { listCompanyshares(request,
			 * response); } if(theCommand != null) { addCompany(request, response);
			 * theCommand = null;
			 * 
			 * }
			 */

		} catch (Exception exc) {
			throw new ServletException(exc);
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request,response);

			}

	private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("loginpage.jsp");
		dispatcher.forward(request, response);		
	}

	private void getEnlistNewCompanyForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//RequestDispatcher dispatcher = request.getRequestDispatcher("EnlistNewCompany.jsp");
		//dispatcher.forward(request, response);
		 response.sendRedirect("EnlistNewCompany.jsp");

		
	}

	private void updateShareDetails(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException, ServletException {
		// read company info from form data
		int companyId = Integer.parseInt(request.getParameter("id"));

		float sharePrice = Float.parseFloat(request.getParameter("sharePrice"));

		// create a new object
		CompanyShare shareObj = new CompanyShare();
		shareObj.setcompanyId(companyId);
		shareObj.setsharePrice(sharePrice);

		// update data in the database
		CompanySharesDao sharedao = new CompanySharesDao();
		sharedao.updateShareDetails(shareObj);

		// send back to main page (the shares list)
		listCompanyshares(request, response);

	}

	private void loadEditForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
		int companyId = Integer.parseInt(request.getParameter("IdToEdit"));

		CompanyShare shareObj = new CompanyShare();
		CompanySharesDao sharedao = new CompanySharesDao();

		shareObj = sharedao.getCompany(companyId);

		request.setAttribute("CompanyDetails", shareObj);

		RequestDispatcher dispatcher = request.getRequestDispatcher("updateShareDetails.jsp");
		dispatcher.forward(request, response);
	}

	private void deleteCompany(HttpServletRequest request, HttpServletResponse response)
			throws ClassNotFoundException, SQLException, IOException, ServletException {
		// read company id from form data
		int companyId = Integer.parseInt(request.getParameter("IdToDelete"));

		// create a new object
		CompanyShare shareObj = new CompanyShare();
		shareObj.setcompanyId(companyId);

		// delete from the database
		CompanySharesDao sharedao = new CompanySharesDao();
		sharedao.deleteCompany(shareObj);

		// send back to main page (the shares list)

		listCompanyshares(request, response);
	}

	private void addCompany(HttpServletRequest request, HttpServletResponse response)
			throws ClassNotFoundException, SQLException, IOException, ServletException {

		// read company info from form data
		String companyName = request.getParameter("companyName");
		float sharePrice = Float.parseFloat(request.getParameter("sharePrice"));

		// create a new object
		CompanyShare shareObj = new CompanyShare(companyName, sharePrice);

		// add to the database
		CompanySharesDao sharedao = new CompanySharesDao();
		sharedao.addShares(shareObj);

		// send back to main page (the student list)
		//listCompanyshares(request, response);

		 response.sendRedirect("ShareController");

	}

	private void listCompanyshares(HttpServletRequest request, HttpServletResponse response)
			throws ClassNotFoundException, SQLException, IOException, ServletException {

		// get company details from db
		CompanySharesDao sharedao = new CompanySharesDao();

		List<CompanyShare> sharesList = sharedao.getShares();

		// add list to the request
		request.setAttribute("ListOfCompanies", sharesList);

		// send to JSP page (view)
		//response.sendRedirect("companyShares.jsp");

		RequestDispatcher dispatcher = request.getRequestDispatcher("companyShares.jsp");
		dispatcher.forward(request, response);

	}

}

package DAO;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

import model.CompanyShare;

public class CompanySharesDao {

	public void addShares(CompanyShare shareObj) throws SQLException, ClassNotFoundException {
		
		Connection myConn = null;
		PreparedStatement pst = null;
		
		try {
			// get db connection
			myConn = GetDbConnection.getDBConnection();
			
			// create sql for insert
			String sql = "insert into Company_share "
					   + "(Company_name, Share_price) "
					   + "values (?, ?)";
			
			pst = myConn.prepareStatement(sql);
			
			// set the param values for the Company 
			pst.setString(1, shareObj.getcompanyName());
			pst.setFloat(2, shareObj.getsharePrice());
			
			// execute sql insert
			pst.executeUpdate();
		}
		finally {
			// clean up JDBC objects
			close(myConn, pst, null);
		}
		
		
	}

	public List<CompanyShare> getShares() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		List<CompanyShare> sharesList = new ArrayList<CompanyShare>();

		Connection myConn = null;
		PreparedStatement pst = null;
		ResultSet Rs = null;
		try {
			// get a connection
			myConn = GetDbConnection.getDBConnection();

			// create sql statement
			String query = "select * from Company_share order by Share_price desc";
			pst = myConn.prepareStatement(query);

			// execute query
			Rs = pst.executeQuery(query);

			// process result set
			while (Rs.next()) {

				// retrieve data from result set row
				int id = Rs.getInt("Company_id");
				String companyName = Rs.getString("Company_name");
				float price = Rs.getFloat("Share_price");

				// create new company shares object
				CompanyShare shareObj = new CompanyShare(id, companyName, price);

				// add it to the list of students
				sharesList.add(shareObj);
			}

			return sharesList;
		} finally {
			// close JDBC objects
			close(myConn, pst, Rs);
		}
	}

	private static void close(Connection myConn, PreparedStatement pst, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
			
			if (pst != null) {
				pst.close();
			}
			
			if (myConn != null) {
				myConn.close();   // doesn't really close it ... just puts back in connection pool
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}		
	}


	public CompanyShare getCompany(int companyId) throws Exception {

		CompanyShare theShare = null;
		Connection myConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;		
		try {
		
			// get connection to database
			myConn = GetDbConnection.getDBConnection();
			
			// create sql to get selected share 
			String sql = "select * from Company_share where Company_Id=?";
			
			// create prepared statement
			pst = myConn.prepareStatement(sql);
			
			// set params
			pst.setInt(1, companyId);
			
			// execute statement
			rs = pst.executeQuery();
			
			// retrieve data from result set row
			if (rs.next()) {
				int companyId1 = rs.getInt("Company_id");
				String companyName = rs.getString("Company_name");
				float price = rs.getFloat("Share_price");

				
				 theShare = new CompanyShare(companyId1, companyName, price);
			}
			else {
				throw new Exception("Could not find student id: " + companyId);
			}				
			
			return theShare;
		}
		finally {
			// clean up JDBC objects
			close(myConn, pst, rs);
		}		
	}

	public static void updateShareDetails(CompanyShare shareObj) throws ClassNotFoundException, SQLException {
		Connection myConn = null;
		PreparedStatement pst = null;
		int companyId =  shareObj.getcompanyId();
		float sharePrice = shareObj.getsharePrice();
		
		try {
			// get db connection
			myConn = GetDbConnection.getDBConnection();
			
			// create sql for insert
			String sql = "UPDATE Company_share SET Share_price =? WHERE Company_id =?";
			pst = myConn.prepareStatement(sql);
			
			// set the param values for the Company 
			pst.setFloat(1, sharePrice);
			pst.setFloat(2, companyId);
			
			// execute sql insert
			pst.executeUpdate();
		}
		finally {
			// clean up JDBC objects
			close(myConn, pst, null);
		}
		
	}

	public void deleteCompany(CompanyShare shareObj) throws ClassNotFoundException, SQLException {
		Connection myConn = null;
		PreparedStatement pst = null;
		
		try {
			// get db connection
			myConn = GetDbConnection.getDBConnection();
			
			// create sql for insert
			String sql = "DELETE FROM Company_share WHERE Company_Id=?";
			
			pst = myConn.prepareStatement(sql);
			
			// set the param values for the Company 
			pst.setInt(1, shareObj.getcompanyId());
			
			// execute sql insert
			pst.executeUpdate();
		}
		finally {
			// clean up JDBC objects
			close(myConn, pst, null);
		}
				
	}

}

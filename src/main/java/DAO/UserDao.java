package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import model.UserModel;

public class UserDao {

	public  void registerUser(UserModel user) throws SQLException, ClassNotFoundException {

		Connection myConn = GetDbConnection.getDBConnection();

		String query = "insert into Users values(?,?,?,?,?,?)";
		PreparedStatement pst = myConn.prepareStatement(query);

		pst.setString(1, user.getFirstName());
		pst.setString(2, user.getLastName());
		pst.setString(3, user.getDob());
		pst.setString(4, user.getGender());
		pst.setString(5, user.getEmail());
		pst.setString(6, user.getPassword());

		pst.executeUpdate();
	}

	public static boolean CheckUserExistance(String accMail) throws SQLException, ClassNotFoundException {
		
			boolean isEmailAlreadyExists=false;
			
			Connection myConn =GetDbConnection.getDBConnection();
			
			
			String query = "select * from Users where email=?";
			
			PreparedStatement pst = myConn.prepareStatement(query);
			pst.setString(1, accMail);
			
		    ResultSet rs = pst.executeQuery();
		    			
			 if(rs.next()) {
				 isEmailAlreadyExists =true;
			  }	
			 else {
				 isEmailAlreadyExists = false;
			 }
			
		 return isEmailAlreadyExists;
	}



	public static boolean validateCredentials(UserModel user) throws ClassNotFoundException, SQLException {
Connection myConn = GetDbConnection.getDBConnection();

        boolean isCredentialsValid = false;
		
		String query = "select * from Users where email=? and password=?";
		PreparedStatement pst = myConn.prepareStatement(query);
		
		pst.setString(1, user.getEmail());
		pst.setString(2, user.getPassword());
		
	    ResultSet rs = pst.executeQuery();
	    
	    if(rs.next()) {
	    	isCredentialsValid = true;
	    }
	    
	    return isCredentialsValid;
	}
	

}

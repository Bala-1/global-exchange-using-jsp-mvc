package model;

public class CompanyShare {
	
    private int companyId;
    private String companyName;
    private float sharePrice;
    
    
	public CompanyShare() {
		super();
		// TODO Auto-generated constructor stub
	}


	public CompanyShare(String companyName, float sharePrice) {
		this.companyName = companyName;
		this.sharePrice = sharePrice;
	}
	


	public CompanyShare(int companyId, String companyName, float sharePrice) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.sharePrice = sharePrice;
	}


	public int getcompanyId() {
		return companyId;
	}


	public void setcompanyId(int companyId) {
		this.companyId = companyId;
	}


	public String getcompanyName() {
		return companyName;
	}


	public void setcompanyName(String companyName) {
		this.companyName = companyName;
	}


	public float getsharePrice() {
		return sharePrice;
	}


	public void setsharePrice(float sharePrice) {
		this.sharePrice = sharePrice;
	}


	@Override
	public String toString() {
		return "CompanyShare [companyId=" + companyId + ", companyName=" + companyName + ", sharePrice=" + sharePrice
				+ "]";
	}
	
}
